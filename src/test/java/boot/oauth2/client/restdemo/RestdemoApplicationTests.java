package boot.oauth2.client.restdemo;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestdemoApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void consistencyCheck() {
		var greeting = new Greeting(1, "Hello Dolly");
		Assert.assertNotNull(greeting);
	}

}
